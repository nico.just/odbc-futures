use inflector::Inflector;
use quote::quote;
use syn::Attribute;
use synstructure::decl_derive;

fn odbc_columns_derive(mut s: synstructure::Structure) -> proc_macro2::TokenStream {
    const ODBC_STRING_UTF8: &'static str = "odbc_string_utf8";

    let mut struct_use_utf8 = false;
    let mut seq_col_number = 0u16;
    let mut caseconv: Option<fn(&String) -> String> = None;

    for struct_meta in s
        .ast()
        .attrs
        .iter()
        .map(Attribute::parse_meta)
        .filter_map(Result::ok)
    {
        if let syn::Meta::Word(name) = &struct_meta {
            let name = name.to_string();
            match name.as_str() {
                ODBC_STRING_UTF8 => struct_use_utf8 = true,
                "odbc_columns_seq" => {
                    seq_col_number = 1;
                }
                "odbc_rename_camel_case" => caseconv = Some(Inflector::to_camel_case),
                "odbc_rename_class_case" => caseconv = Some(Inflector::to_class_case),
                "odbc_rename_kebab_case" => caseconv = Some(Inflector::to_kebab_case),
                "odbc_rename_train_case" => caseconv = Some(Inflector::to_train_case),
                "odbc_rename_screaming_snake_case" => {
                    caseconv = Some(Inflector::to_screaming_snake_case)
                }
                "odbc_rename_table_case" => caseconv = Some(Inflector::to_table_case),
                "odbc_rename_sentence_case" => caseconv = Some(Inflector::to_sentence_case),
                "odbc_rename_snake_case" => caseconv = Some(Inflector::to_snake_case),
                "odbc_rename_pascal_case" => caseconv = Some(Inflector::to_pascal_case),
                "odbc_rename_foreign_key" => caseconv = Some(Inflector::to_foreign_key),
                "odbc_rename_pluralize" => caseconv = Some(Inflector::to_plural),
                "odbc_rename_singularize" => caseconv = Some(Inflector::to_singular),
                _ => {}
            }
        }
    }

    s.filter(|bi|{
        for field_meta in bi.ast().attrs.iter().map(Attribute::parse_meta).filter_map(Result::ok) {
            if let syn::Meta::Word(name) = &field_meta {
                if name == "odbc_ignore" {
                    return false;
                }
            }
        }
        true
    });

    let body = s.bind_with(|_| synstructure::BindStyle::RefMut).each(|bi| {
        let field = bi.ast();

        let mut field_use_utf8 = false;
        let mut field_force_use_utf16 = false;
        let mut use_time2: Option<bool> = None;

        let mut col_name = field.ident.as_ref().map(ToString::to_string);
        if let Some(caseconv) = caseconv {
            col_name = col_name.as_ref().map(caseconv);
        }

        let mut col_number: Option<u16> = None;
        let mut visit_nested = false;

        for field_meta in field
            .attrs
            .iter()
            .map(Attribute::parse_meta)
            .filter_map(Result::ok)
        {
            match &field_meta {
                syn::Meta::Word(name) if name == ODBC_STRING_UTF8 => field_use_utf8 = true,
                syn::Meta::Word(name) if name == "odbc_string_utf16" => field_force_use_utf16 = true,
                syn::Meta::Word(name) if name == "odbc_nested" => {
                    visit_nested = true;
                    break;
                }
                syn::Meta::NameValue(name_value) if name_value.ident == "odbc_time" => {
                    match &name_value.lit {
                        syn::Lit::Str(name) if name.value() == "time" => use_time2 = Some(false),
                        syn::Lit::Str(name) if name.value() == "time2" => use_time2 = Some(true),
                        x => panic!("not a valid odbc_time name {:?}", x),
                    }
                }
                syn::Meta::NameValue(name_value) if name_value.ident == "odbc_col_name" => {
                    match &name_value.lit {
                        syn::Lit::Str(name) => col_name = Some(name.value()),
                        x => panic!("not a valid column name {:?}", x),
                    }
                }
                syn::Meta::NameValue(name_value) if name_value.ident == "odbc_col_number" => {
                    match &name_value.lit {
                        syn::Lit::Int(number) => {
                            let val = number.value();
                            assert!(val > 0, "column numbers start at 1");
                            assert!(val <= std::u16::MAX as u64);
                            col_number = Some(val as u16);
                        }
                        x => panic!("not a valid column number {:?}", x),
                    }
                }
                _ => {}
            }
        }

        if visit_nested {
            return quote! {
                if let Some(poll) = #bi.visit(visitor) {
                    return Some(poll);
                }
            }
        }

        assert!(!field_force_use_utf16 || !field_use_utf8);

        let ty = &field.ty;
        let field_type: String = quote!(#ty).to_string();
        let use_string_context = field_type == "String"
            || field_type == "Option < String >"
            || field_use_utf8
            || field_force_use_utf16;

        let context_type = if use_string_context {
            if (struct_use_utf8 || field_use_utf8) && !field_force_use_utf16 {
                quote!(())
            } else {
                quote!(Vec<u16>)
            }
        } else if let Some(time2) = use_time2 {
          if time2 {
              quote!(odbc_sys::SQL_SS_TIME2_STRUCT)
          } else {
              quote!(odbc_sys::SQL_TIME_STRUCT)
          }
        } else {
            quote!(_)
        };

        let accept = quote! {
            return Some( visitor.accept::< _, #context_type >(#bi) );
        };

        if let Some(col_number) = col_number {
            quote! {
                if visitor.col_number() == #col_number {
                    #accept
                }
            }
        } else if seq_col_number > 0 {
            let val = seq_col_number;
            seq_col_number += 1;
            quote! {
                if visitor.col_number() == #val {
                    #accept
                }
            }
        } else if let Some(col_name) = col_name {
            quote! {
                if visitor.col_name() == #col_name {
                    #accept
                }
            }
        } else {
            panic!("failed to setup binding for field {:?}", field)
        }
    });

    let in_self = std::env::var("CARGO_PKG_NAME").unwrap() == "odbc-futures";

    let trait_name = if in_self {
        quote!(crate::SqlColumns)
    } else {
        quote!(odbc_futures::SqlColumns)
    };
    let crate_name = if in_self {
        quote!(crate)
    } else {
        quote!(odbc_futures)
    };

    let mut stream = s.bound_impl(trait_name, quote! {
        fn visit(&mut self, visitor: &mut #crate_name::SqlColumnVisitor) -> Option< #crate_name::SqlPoll > {
            extern crate futures;
            extern crate odbc_sys;

            match *self { #body };
            None
        }
    });
    if in_self {
        let string = stream.to_string().replace("extern crate crate ;", "");
        use std::str::FromStr;
        stream = proc_macro2::TokenStream::from_str(&string).unwrap();
    }
    //eprintln!("{}", synstructure::unpretty_print(&stream));
    stream
}

decl_derive!(
    [Odbc, attributes(
    odbc_string_utf8,
    odbc_columns_seq,
    odbc_rename_camel_case,
    odbc_rename_class_case,
    odbc_rename_kebab_case,
    odbc_rename_train_case,
    odbc_rename_screaming_snake_case,
    odbc_rename_table_case,
    odbc_rename_sentence_case,
    odbc_rename_snake_case,
    odbc_rename_pascal_case,
    odbc_rename_foreign_key,
    odbc_rename_pluralize,
    odbc_rename_singularize,
    odbc_string_utf16,
    odbc_time,
    odbc_nested,
    odbc_ignore,
    odbc_col_name,
    odbc_col_number,
    )] => odbc_columns_derive
);
