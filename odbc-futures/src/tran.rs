use crate::error::SqlResult;
use crate::handle::SqlHandle;
use crate::poll::*;
use futures::Async;
use odbc_sys::*;

fn end_transaction_impl<T: SqlHandle>(handle: &mut T, commit: bool) -> SqlPoll {
    let value = if commit { SQL_COMMIT } else { SQL_ROLLBACK };

    let ret = unsafe { SQLEndTran(T::TYPE, handle.handle(), value) };

    match ret {
        SQL_SUCCESS | SQL_SUCCESS_WITH_INFO => Ok(Async::Ready(())),
        SQL_STILL_EXECUTING => Ok(Async::NotReady),
        SQL_ERROR => Err(handle.get_detailed_error(ret)),
        _ => panic!("Unexpected SQLEndTran return code: {:?}", ret),
    }
}

pub trait SqlEndTransaction: SqlHandle {
    fn commit(&mut self) -> SqlResult {
        end_transaction_impl(self, true).unwrap_async()
    }

    fn rollback(&mut self) -> SqlResult {
        end_transaction_impl(self, false).unwrap_async()
    }

    fn end_transaction(&mut self, commit: bool) -> SqlResult {
        end_transaction_impl(self, commit).unwrap_async()
    }

    fn commit_async(self) -> SqlFuture<Self> {
        self.end_transaction_async(true)
    }

    fn rollback_async(self) -> SqlFuture<Self> {
        self.end_transaction_async(false)
    }

    fn end_transaction_async(self, commit: bool) -> SqlFuture<Self> {
        let f = SqlValueFuture::new(self, move |handle| end_transaction_impl(handle, commit));

        flatten_value_future(f)
    }
}
