use super::*;
use crate::{SqlColumn, SqlGetDataExtensions};
use futures::Async;

#[derive(Debug, Copy, Eq, PartialEq, Clone)]
enum SqlColumnVisitorAction {
    Clean,
    Bind,
    GetData,
    FlushContext,
    FlushOptional,
}

pub(crate) struct SqlVisitor {
    columns: Vec<SqlColumn>,
    data: Vec<SqlValueData>,
    current_column_index: usize,
    long_column_index: usize,
    has_get_data_any_column_extension: bool,
}

impl SqlVisitor {
    pub fn new(statement: &SqlStatement, columns: Vec<SqlColumn>) -> SqlResult<SqlVisitor> {
        assert!(!columns.is_empty());
        let len = columns.len();

        let has_get_data_any_column_extension = {
            let connection = statement.connection();
            let lock = connection.try_read().unwrap();
            let extensions = lock.get_info_getdata_extensions()?;
            extensions.contains(SqlGetDataExtensions::ANY_COLUMN)
        };

        let long_column_index = columns
            .iter()
            .position(|column| column.col_size == 0)
            .unwrap_or(len);

        Ok(SqlVisitor {
            columns,
            long_column_index,
            has_get_data_any_column_extension,
            current_column_index: long_column_index,
            data: vec![Default::default(); len],
        })
    }

    fn run_all<T: SqlColumns>(
        &mut self,
        action: SqlColumnVisitorAction,
        statement: &mut SqlStatement,
        value: &mut T,
    ) -> SqlResult {
        use self::SqlColumnVisitorAction::*;
        self.current_column_index = 0;

        let end = if action == Bind && !self.has_get_data_any_column_extension {
            self.long_column_index
        } else {
            self.columns.len()
        };

        while self.current_column_index < end {
            let column = &self.columns[self.current_column_index];
            if action == Bind && column.col_size == 0 {
                self.current_column_index += 1;
                continue;
            }

            let data = &mut self.data[self.current_column_index];
            let mut visitor = SqlColumnVisitor {
                column,
                data,
                statement,
                action,
            };

            if let Some(Err(e)) = value.visit(&mut visitor) {
                self.current_column_index = 0;
                return Err(e);
            }

            self.current_column_index += 1;
        }

        self.current_column_index = self.long_column_index;
        Ok(())
    }

    pub fn bind<T: SqlColumns>(
        &mut self,
        statement: &mut SqlStatement,
        value: &mut T,
    ) -> SqlResult {
        self.run_all(SqlColumnVisitorAction::Bind, statement, value)
    }

    pub fn flush_context<T: SqlColumns>(
        &mut self,
        statement: &mut SqlStatement,
        value: &mut T,
    ) -> SqlResult {
        self.run_all(SqlColumnVisitorAction::FlushContext, statement, value)
    }

    pub fn flush_optional<T: SqlColumns>(
        &mut self,
        statement: &mut SqlStatement,
        value: &mut T,
    ) -> SqlResult {
        self.run_all(SqlColumnVisitorAction::FlushOptional, statement, value)
    }

    pub fn clean<T: SqlColumns>(
        &mut self,
        statement: &mut SqlStatement,
        value: &mut T,
    ) -> SqlResult {
        self.run_all(SqlColumnVisitorAction::Clean, statement, value)
    }

    pub fn get_data<T: SqlColumns>(
        &mut self,
        statement: &mut SqlStatement,
        value: &mut T,
    ) -> SqlPoll {
        while self.current_column_index < self.columns.len() {
            let column = &self.columns[self.current_column_index];

            if self.has_get_data_any_column_extension && column.col_size > 0 {
                self.current_column_index += 1;
                continue;
            }

            let data = &mut self.data[self.current_column_index];

            let mut visitor = SqlColumnVisitor {
                column,
                data,
                statement,
                action: SqlColumnVisitorAction::GetData,
            };

            match value.visit(&mut visitor) {
                Some(poll) => match poll {
                    Ok(Async::Ready(())) => self.current_column_index += 1,
                    Err(e) => {
                        self.current_column_index = self.long_column_index;
                        return Err(e);
                    }
                    not_ready => return not_ready,
                },
                None => self.current_column_index += 1
            }
        }
        self.current_column_index = self.long_column_index;
        Ok(Async::Ready(()))
    }
}

pub struct SqlColumnVisitor<'a, 'b: 'a> {
    action: SqlColumnVisitorAction,
    statement: &'b mut SqlStatement,
    column: &'a SqlColumn,
    data: &'a mut SqlValueData,
}

impl<'a, 'b: 'a> SqlColumnVisitor<'a, 'b> {
    pub fn accept<TValue, TContext>(&mut self, value: &mut TValue) -> SqlPoll
    where
        TValue: SqlValue<TContext>,
        SqlValueContext: AsMut<TContext>,
    {
        use self::SqlColumnVisitorAction::*;
        match self.action {
            Bind => self.bind(value),
            GetData => self.get_data(value),
            FlushContext => self.flush_context(value),
            FlushOptional => self.flush_optional(value),
            Clean => self.clean(value),
        }
    }

    pub fn col_number(&self) -> SQLUSMALLINT {
        self.column.col_number
    }

    pub fn col_name(&self) -> &str {
        self.column.col_name.as_str()
    }

    fn bind<TValue, TContext>(&mut self, value: &mut TValue) -> SqlPoll
    where
        TValue: SqlValue<TContext>,
        SqlValueContext: AsMut<TContext>,
    {
        value
            .bind_column(
                self.column,
                self.statement,
                self.data.context.as_mut(),
                &mut self.data.len,
            )
            .map(Async::Ready)
    }

    fn get_data<TValue, TContext>(&mut self, value: &mut TValue) -> SqlPoll
    where
        TValue: SqlValue<TContext>,
        SqlValueContext: AsMut<TContext>,
    {
        value.get_data(
            self.column,
            self.statement,
            self.data.context.as_mut(),
            &mut self.data.len,
        )
    }

    fn flush_context<TValue, TContext>(&mut self, value: &mut TValue) -> SqlPoll
    where
        TValue: SqlValue<TContext>,
        SqlValueContext: AsMut<TContext>,
    {
        assert!(self.data.len >= SQL_NULL_DATA);
        if let Err(e) = value.flush_context(self.data.context.as_mut(), &mut self.data.len) {
            Err(e)
        } else {
            self.data.context.clear();
            Ok(Async::Ready(()))
        }
    }

    fn clean<TValue, TContext>(&mut self, value: &mut TValue) -> SqlPoll
    where
        TValue: SqlValue<TContext>,
        SqlValueContext: AsMut<TContext>,
    {
        value.clean(self.data.context.as_mut()).map(Async::Ready)
    }

    fn flush_optional<TValue, TContext>(&mut self, value: &mut TValue) -> SqlPoll
    where
        TValue: SqlValue<TContext>,
        SqlValueContext: AsMut<TContext>,
    {
        assert!(self.data.len >= SQL_NULL_DATA);
        if let Err(e) = value.flush_optional(&mut self.data.len) {
            Err(e)
        } else {
            self.data.len = SQL_NULL_DATA;
            Ok(Async::Ready(()))
        }
    }
}
