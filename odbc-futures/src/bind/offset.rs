use super::*;
use crate::util;
use chrono::{DateTime, FixedOffset, NaiveDate, NaiveDateTime, NaiveTime};

impl SqlValue<SQL_SS_TIMESTAMPOFFSET_STRUCT> for DateTime<FixedOffset> {
    fn clean(&mut self, context: &mut SQL_SS_TIMESTAMPOFFSET_STRUCT) -> SqlResult {
        *self = SqlDefault::default();
        *context = Default::default();
        Ok(())
    }

    fn bind_column(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut SQL_SS_TIMESTAMPOFFSET_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        let ret = unsafe {
            SQLBindCol(
                statement.typed_handle(),
                column.col_number,
                <DateTime<FixedOffset>>::C_DATA_TYPE,
                (context as *mut SQL_SS_TIMESTAMPOFFSET_STRUCT) as SQLPOINTER,
                std::mem::size_of::<SQL_SS_TIMESTAMPOFFSET_STRUCT>() as SQLLEN,
                indicator,
            )
        };
        check_bind_column_result(statement, ret)
    }
    fn get_data(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut SQL_SS_TIMESTAMPOFFSET_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlPoll {
        let ret = unsafe {
            SQLGetData(
                statement.typed_handle(),
                column.col_number,
                <DateTime<FixedOffset>>::C_DATA_TYPE,
                (context as *mut SQL_SS_TIMESTAMPOFFSET_STRUCT) as SQLPOINTER,
                std::mem::size_of::<SQL_SS_TIMESTAMPOFFSET_STRUCT>() as SQLLEN,
                indicator,
            )
        };
        check_get_data_status_code(statement, ret)
    }

    fn bind_parameter(
        &mut self,
        parameter_number: SQLUSMALLINT,
        options: SqlBindParameterOptions,
        statement: &SqlStatement,
        context: &mut SQL_SS_TIMESTAMPOFFSET_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        *indicator = std::mem::size_of::<SQL_SS_TIMESTAMPOFFSET_STRUCT>() as SQLLEN;
        use chrono::{Datelike, Timelike};

        let offset: i32 = self.offset().local_minus_utc();
        let local = self.naive_local();

        let year = local.year();
        assert!(year >= std::i16::MIN.into() && year <= std::i16::MAX.into());
        let month = local.month();
        assert!(month >= std::u16::MIN.into() && month <= std::u16::MAX.into());
        let day = local.day();
        assert!(day >= std::u16::MIN.into() && day <= std::u16::MAX.into());
        let hour = local.hour();
        assert!(hour >= std::u16::MIN.into() && hour <= std::u16::MAX.into());
        let minute = local.minute();
        assert!(minute >= std::u16::MIN.into() && minute <= std::u16::MAX.into());
        let second = local.second();
        assert!(second >= std::u16::MIN.into() && second <= std::u16::MAX.into());
        let fraction = local.nanosecond();

        let timezone_hour = offset / 3600;
        assert!(timezone_hour >= std::i16::MIN.into() && timezone_hour <= std::i16::MAX.into());

        let timezone_minute = (offset % 3600) / 60;
        assert!(timezone_minute >= std::i16::MIN.into() && timezone_minute <= std::i16::MAX.into());

        let decimal_digits = options.get_decimal_digits::<DateTime<FixedOffset>>();

        *context = SQL_SS_TIMESTAMPOFFSET_STRUCT {
            year: year as SQLSMALLINT,
            month: month as SQLUSMALLINT,
            day: day as SQLUSMALLINT,
            hour: hour as SQLUSMALLINT,
            minute: minute as SQLUSMALLINT,
            second: second as SQLUSMALLINT,
            fraction: util::truncate_nanoseconds(fraction as SQLUINTEGER, decimal_digits),
            timezone_hour: timezone_hour as SQLSMALLINT,
            timezone_minute: timezone_minute as SQLSMALLINT,
        };

        let ret = unsafe {
            SQLBindParameter(
                statement.typed_handle(),
                parameter_number,
                SQL_PARAM_INPUT,
                <DateTime<FixedOffset>>::C_DATA_TYPE,
                options.get_parameter_type::<DateTime<FixedOffset>>(),
                options.precision,
                decimal_digits,
                (context as *mut SQL_SS_TIMESTAMPOFFSET_STRUCT) as SQLPOINTER,
                std::mem::size_of::<SQL_SS_TIMESTAMPOFFSET_STRUCT>() as SQLLEN,
                indicator,
            )
        };
        check_bind_parameter_result(statement, ret)
    }

    fn flush_context(
        &mut self,
        context: &mut SQL_SS_TIMESTAMPOFFSET_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        if *indicator == SQL_NULL_DATA {
            *self = SqlDefault::default();
        } else {
            let offset = FixedOffset::east(
                i32::from(context.timezone_hour) * 3600 + i32::from(context.timezone_minute) * 60,
            );
            let date = NaiveDate::from_ymd(
                i32::from(context.year),
                u32::from(context.month),
                u32::from(context.day),
            );

            let time = NaiveTime::from_hms_nano(
                u32::from(context.hour),
                u32::from(context.minute),
                u32::from(context.second),
                u32::from(context.fraction),
            );
            let local_datetime = NaiveDateTime::new(date, time);
            let utc_datetime = local_datetime - offset;

            *self = DateTime::from_utc(utc_datetime, offset);
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn visit() {
        use odbc_futures_derive::Odbc;

        #[allow(dead_code)]
        #[derive(Copy, Clone, Odbc)]
        struct Offset {
            a: DateTime<FixedOffset>,
            b: Option<DateTime<FixedOffset>>,
        }

        impl Default for Offset {
            fn default() -> Offset {
                Offset {
                    a: SqlDefault::default(),
                    b: None,
                }
            }
        }
    }
}
