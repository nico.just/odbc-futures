use super::*;
use crate::util;
use chrono::{NaiveDate, NaiveDateTime, NaiveTime};

impl SqlValue<SQL_TIMESTAMP_STRUCT> for NaiveDateTime {
    fn clean(&mut self, context: &mut SQL_TIMESTAMP_STRUCT) -> SqlResult {
        *self = SqlDefault::default();
        *context = Default::default();
        Ok(())
    }

    fn bind_column(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut SQL_TIMESTAMP_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        let ret = unsafe {
            SQLBindCol(
                statement.typed_handle(),
                column.col_number,
                NaiveDateTime::C_DATA_TYPE,
                (context as *mut SQL_TIMESTAMP_STRUCT) as SQLPOINTER,
                std::mem::size_of::<SQL_TIMESTAMP_STRUCT>() as SQLLEN,
                indicator,
            )
        };
        check_bind_column_result(statement, ret)
    }
    fn get_data(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut SQL_TIMESTAMP_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlPoll {
        let ret = unsafe {
            SQLGetData(
                statement.typed_handle(),
                column.col_number,
                NaiveDateTime::C_DATA_TYPE,
                (context as *mut SQL_TIMESTAMP_STRUCT) as SQLPOINTER,
                std::mem::size_of::<SQL_TIMESTAMP_STRUCT>() as SQLLEN,
                indicator,
            )
        };
        check_get_data_status_code(statement, ret)
    }

    fn bind_parameter(
        &mut self,
        parameter_number: SQLUSMALLINT,
        options: SqlBindParameterOptions,
        statement: &SqlStatement,
        context: &mut SQL_TIMESTAMP_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        *indicator = std::mem::size_of::<SQL_TIMESTAMP_STRUCT>() as SQLLEN;
        use chrono::{Datelike, Timelike};

        let year = self.year();
        assert!(year >= std::i16::MIN.into() && year <= std::i16::MAX.into());
        let month = self.month();
        assert!(month >= std::u16::MIN.into() && month <= std::u16::MAX.into());
        let day = self.day();
        assert!(day >= std::u16::MIN.into() && day <= std::u16::MAX.into());
        let hour = self.hour();
        assert!(hour >= std::u16::MIN.into() && hour <= std::u16::MAX.into());
        let minute = self.minute();
        assert!(minute >= std::u16::MIN.into() && minute <= std::u16::MAX.into());
        let second = self.second();
        assert!(second >= std::u16::MIN.into() && second <= std::u16::MAX.into());
        let fraction = self.nanosecond();

        let decimal_digits = options.get_decimal_digits::<NaiveDateTime>();

        *context = SQL_TIMESTAMP_STRUCT {
            year: year as SQLSMALLINT,
            month: month as SQLUSMALLINT,
            day: day as SQLUSMALLINT,
            hour: hour as SQLUSMALLINT,
            minute: minute as SQLUSMALLINT,
            second: second as SQLUSMALLINT,
            fraction: util::truncate_nanoseconds(fraction as SQLUINTEGER, decimal_digits),
        };

        let ret = unsafe {
            SQLBindParameter(
                statement.typed_handle(),
                parameter_number,
                SQL_PARAM_INPUT,
                NaiveDateTime::C_DATA_TYPE,
                options.get_parameter_type::<NaiveDateTime>(),
                options.precision,
                decimal_digits,
                (context as *mut SQL_TIMESTAMP_STRUCT) as SQLPOINTER,
                std::mem::size_of::<SQL_TIMESTAMP_STRUCT>() as SQLLEN,
                indicator,
            )
        };
        check_bind_parameter_result(statement, ret)
    }

    fn flush_context(
        &mut self,
        context: &mut SQL_TIMESTAMP_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        if *indicator == SQL_NULL_DATA {
            *self = SqlDefault::default();
        } else {
            let date = NaiveDate::from_ymd(
                i32::from(context.year),
                u32::from(context.month),
                u32::from(context.day),
            );
            let time = NaiveTime::from_hms_nano(
                u32::from(context.hour),
                u32::from(context.minute),
                u32::from(context.second),
                u32::from(context.fraction),
            );

            *self = NaiveDateTime::new(date, time);
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn visit() {
        use odbc_futures_derive::Odbc;

        #[allow(dead_code)]
        #[derive(Copy, Clone, Odbc)]
        struct DateTime {
            a: NaiveDateTime,
            b: Option<NaiveDateTime>,
        }
        impl Default for DateTime {
            fn default() -> DateTime {
                DateTime {
                    a: SqlDefault::default(),
                    b: None,
                }
            }
        }
    }
}
