use crate::{SqlColumn, SqlDefault, SqlHandle, SqlPoll, SqlResult, SqlStatement, SqlType};
use futures::Async;
use odbc_sys::*;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct SqlBindParameterOptions {
    pub input_output_type: InputOutput,
    pub parameter_type: SqlDataType,
    pub decimal_digits: SQLSMALLINT,
    pub precision: SQLULEN,
}

impl SqlBindParameterOptions {
    fn get_parameter_type<T: SqlType>(&self) -> SqlDataType {
        if self.parameter_type == SQL_UNKNOWN_TYPE {
            T::SQL_DATA_TYPE
        } else {
            self.parameter_type
        }
    }

    fn get_decimal_digits<T: SqlType>(&self) -> SQLSMALLINT {
        if self.decimal_digits < 0 {
            crate::util::decimal_digits(self.get_parameter_type::<T>())
        } else {
            self.decimal_digits
        }
    }
}

impl Default for SqlBindParameterOptions {
    fn default() -> SqlBindParameterOptions {
        SqlBindParameterOptions {
            input_output_type: SQL_PARAM_INPUT,
            parameter_type: SQL_UNKNOWN_TYPE,
            decimal_digits: -1,
            precision: 0,
        }
    }
}

mod buf;
#[cfg(feature = "chrono")]
mod date;
#[cfg(feature = "chrono")]
mod datetime;
#[cfg(feature = "chrono")]
mod offset;
mod pod;
#[cfg(feature = "chrono")]
mod time;

#[cfg(feature = "uuid")]
mod guid;

pub(crate) use self::visitor::SqlVisitor;

#[derive(Debug, Clone)]
pub struct SqlValueData {
    pub len: SQLLEN,
    pub context: SqlValueContext,
}

impl Default for SqlValueData {
    fn default() -> SqlValueData {
        SqlValueData {
            len: SQL_NULL_DATA,
            context: Default::default(),
        }
    }
}

#[derive(Debug, Clone)]
pub enum SqlValueContext {
    None(()),
    Unicode(Vec<u16>),
    NonUnicode(Vec<u8>),
    #[cfg(feature = "uuid")]
    Uuid(SQLGUID),
    #[cfg(feature = "chrono")]
    NaiveDate(SQL_DATE_STRUCT),
    #[cfg(feature = "chrono")]
    NaiveTime(SQL_TIME_STRUCT),
    #[cfg(feature = "chrono")]
    NaiveTime2(SQL_SS_TIME2_STRUCT),
    #[cfg(feature = "chrono")]
    NaiveDateTime(SQL_TIMESTAMP_STRUCT),
    #[cfg(feature = "chrono")]
    DateTime(SQL_SS_TIMESTAMPOFFSET_STRUCT),
}

impl SqlValueContext {
    fn clear(&mut self) {
        use self::SqlValueContext::*;
        match self {
            None(_) => {}
            NonUnicode(x) => x.clear(),
            Unicode(x) => x.clear(),
            #[cfg(feature = "uuid")]
            Uuid(x) => *x = Default::default(),
            #[cfg(feature = "chrono")]
            NaiveDate(x) => *x = Default::default(),
            #[cfg(feature = "chrono")]
            NaiveTime(x) => *x = Default::default(),
            #[cfg(feature = "chrono")]
            NaiveTime2(x) => *x = Default::default(),
            #[cfg(feature = "chrono")]
            NaiveDateTime(x) => *x = Default::default(),
            #[cfg(feature = "chrono")]
            DateTime(x) => *x = Default::default(),
        }
    }
}

impl Default for SqlValueContext {
    fn default() -> SqlValueContext {
        SqlValueContext::None(())
    }
}

macro_rules! context_as_mut {
    ($t:ty, $n:ident) => {
        impl AsMut<$t> for SqlValueContext {
            fn as_mut(&mut self) -> &mut $t {
                match self {
                    SqlValueContext::$n(x) => return x,
                    _ => *self = SqlValueContext::$n(Default::default()),
                }
                match self {
                    SqlValueContext::$n(x) => x,
                    _ => unreachable!(),
                }
            }
        }
    };
}

context_as_mut!((), None);
context_as_mut!(Vec<u16>, Unicode);
context_as_mut!(Vec<u8>, NonUnicode);
#[cfg(feature = "uuid")]
context_as_mut!(SQLGUID, Uuid);
#[cfg(feature = "chrono")]
context_as_mut!(SQL_DATE_STRUCT, NaiveDate);
#[cfg(feature = "chrono")]
context_as_mut!(SQL_TIME_STRUCT, NaiveTime);
#[cfg(feature = "chrono")]
context_as_mut!(SQL_SS_TIME2_STRUCT, NaiveTime2);
#[cfg(feature = "chrono")]
context_as_mut!(SQL_TIMESTAMP_STRUCT, NaiveDateTime);
#[cfg(feature = "chrono")]
context_as_mut!(SQL_SS_TIMESTAMPOFFSET_STRUCT, DateTime);

mod visitor;
pub use self::visitor::SqlColumnVisitor;

pub trait SqlColumns: Default + Clone + Send + 'static {
    fn visit(&mut self, visitor: &mut SqlColumnVisitor) -> Option<SqlPoll>;
}

pub trait SqlValue<T>
where
    SqlValueContext: AsMut<T>,
{
    fn clean(&mut self, context: &mut T) -> SqlResult;

    fn bind_column(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut T,
        indicator: &mut SQLLEN,
    ) -> SqlResult;
    fn get_data(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut T,
        indicator: &mut SQLLEN,
    ) -> SqlPoll;

    fn bind_parameter(
        &mut self,
        parameter_number: SQLUSMALLINT,
        options: SqlBindParameterOptions,
        statement: &SqlStatement,
        context: &mut T,
        indicator: &mut SQLLEN,
    ) -> SqlResult;

    fn flush_context(&mut self, context: &mut T, indicator: &mut SQLLEN) -> SqlResult;

    fn flush_optional(&mut self, _indicator: &mut SQLLEN) -> SqlResult {
        Ok(())
    }
}

impl<TValue, TContext> SqlValue<TContext> for Option<TValue>
where
    TValue: SqlValue<TContext> + SqlType + SqlDefault,
    SqlValueContext: AsMut<TContext>,
{
    fn clean(&mut self, context: &mut TContext) -> SqlResult {
        self.get_or_insert_with(SqlDefault::default).clean(context)
    }

    fn bind_column(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut TContext,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        self.get_or_insert_with(SqlDefault::default)
            .bind_column(column, statement, context, indicator)
    }

    fn get_data(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut TContext,
        indicator: &mut SQLLEN,
    ) -> SqlPoll {
        self.get_or_insert_with(SqlDefault::default)
            .get_data(column, statement, context, indicator)
    }

    fn bind_parameter(
        &mut self,
        parameter_number: SQLUSMALLINT,
        options: SqlBindParameterOptions,
        statement: &SqlStatement,
        context: &mut TContext,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        match options.input_output_type {
            SQL_PARAM_INPUT => {
                if let Some(value) = self {
                    value.bind_parameter(parameter_number, options, statement, context, indicator)
                } else {
                    *indicator = SQL_NULL_DATA;
                    let ret = unsafe {
                        SQLBindParameter(
                            statement.typed_handle(),
                            parameter_number,
                            SQL_PARAM_INPUT,
                            TValue::C_DATA_TYPE,
                            options.get_parameter_type::<TValue>(),
                            options.precision,
                            options.get_decimal_digits::<TValue>(),
                            std::ptr::null_mut(),
                            0,
                            indicator,
                        )
                    };
                    check_bind_parameter_result(statement, ret)
                }
            }
            SQL_PARAM_OUTPUT | SQL_PARAM_INPUT_OUTPUT => self
                .get_or_insert_with(SqlDefault::default)
                .bind_parameter(parameter_number, options, statement, context, indicator),
            _ => unimplemented!(),
        }
    }

    fn flush_context(&mut self, context: &mut TContext, indicator: &mut SQLLEN) -> SqlResult {
        self.get_or_insert_with(SqlDefault::default)
            .flush_context(context, indicator)
    }

    fn flush_optional(&mut self, indicator: &mut SQLLEN) -> SqlResult {
        if *indicator == SQL_NULL_DATA {
            self.take();
        }
        Ok(())
    }
}

fn check_bind_parameter_result(statement: &SqlStatement, ret: SQLRETURN) -> SqlResult {
    check_bind_status_code(statement, ret, "SQLBindParameter")
}

fn check_bind_column_result(statement: &SqlStatement, ret: SQLRETURN) -> SqlResult {
    check_bind_status_code(statement, ret, "SQLBindCol")
}

fn check_get_data_status_code(statement: &SqlStatement, ret: SQLRETURN) -> SqlPoll {
    match ret {
        SQL_SUCCESS | SQL_NO_DATA | SQL_SUCCESS_WITH_INFO => Ok(Async::Ready(())),
        SQL_STILL_EXECUTING => Ok(Async::NotReady),
        SQL_ERROR => {
            let e = statement.get_detailed_error(ret);
            Err(e)
        }
        _ => panic!("Unexpected SQLGetData return code: {:?}", ret),
    }
}

fn check_bind_status_code(statement: &SqlStatement, ret: SQLRETURN, f: &str) -> SqlResult {
    match ret {
        SQL_SUCCESS | SQL_SUCCESS_WITH_INFO => Ok(()),
        SQL_ERROR => {
            let e = statement.get_detailed_error(ret);
            Err(e)
        }
        _ => panic!("Unexpected {} return code: {:?}", f, ret),
    }
}
