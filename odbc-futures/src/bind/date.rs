use super::*;
use crate::SqlType;
use chrono::NaiveDate;

impl SqlValue<SQL_DATE_STRUCT> for NaiveDate {
    fn clean(&mut self, context: &mut SQL_DATE_STRUCT) -> SqlResult {
        *self = SqlDefault::default();
        *context = Default::default();
        Ok(())
    }

    fn bind_column(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut SQL_DATE_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        let ret = unsafe {
            SQLBindCol(
                statement.typed_handle(),
                column.col_number,
                NaiveDate::C_DATA_TYPE,
                (context as *mut SQL_DATE_STRUCT) as SQLPOINTER,
                std::mem::size_of::<SQL_DATE_STRUCT>() as SQLLEN,
                indicator,
            )
        };
        check_bind_column_result(statement, ret)
    }
    fn get_data(
        &mut self,
        column: &SqlColumn,
        statement: &mut SqlStatement,
        context: &mut SQL_DATE_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlPoll {
        let ret = unsafe {
            SQLGetData(
                statement.typed_handle(),
                column.col_number,
                NaiveDate::C_DATA_TYPE,
                (context as *mut SQL_DATE_STRUCT) as SQLPOINTER,
                std::mem::size_of::<SQL_DATE_STRUCT>() as SQLLEN,
                indicator,
            )
        };
        check_get_data_status_code(statement, ret)
    }

    fn bind_parameter(
        &mut self,
        parameter_number: SQLUSMALLINT,
        options: SqlBindParameterOptions,
        statement: &SqlStatement,
        context: &mut SQL_DATE_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        *indicator = std::mem::size_of::<SQL_DATE_STRUCT>() as SQLLEN;
        use chrono::Datelike;
        let year = self.year();
        assert!(year >= std::i16::MIN.into() && year <= std::i16::MAX.into());
        let month = self.month();
        assert!(month >= std::u16::MIN.into() && month <= std::u16::MAX.into());
        let day = self.day();
        assert!(day >= std::u16::MIN.into() && day <= std::u16::MAX.into());

        *context = SQL_DATE_STRUCT {
            year: year as SQLSMALLINT,
            month: month as SQLUSMALLINT,
            day: day as SQLUSMALLINT,
        };

        let ret = unsafe {
            SQLBindParameter(
                statement.typed_handle(),
                parameter_number,
                SQL_PARAM_INPUT,
                NaiveDate::C_DATA_TYPE,
                options.get_parameter_type::<NaiveDate>(),
                options.precision,
                options.get_decimal_digits::<NaiveDate>(),
                (context as *mut SQL_DATE_STRUCT) as SQLPOINTER,
                std::mem::size_of::<SQL_DATE_STRUCT>() as SQLLEN,
                indicator,
            )
        };
        check_bind_parameter_result(statement, ret)
    }

    fn flush_context(
        &mut self,
        context: &mut SQL_DATE_STRUCT,
        indicator: &mut SQLLEN,
    ) -> SqlResult {
        if *indicator == SQL_NULL_DATA {
            *self = SqlDefault::default();
        } else {
            *self = NaiveDate::from_ymd(
                i32::from(context.year),
                u32::from(context.month),
                u32::from(context.day),
            );
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn visit() {
        use odbc_futures_derive::Odbc;

        #[allow(dead_code)]
        #[derive(Copy, Clone, Odbc)]
        struct Date {
            a: NaiveDate,
            b: Option<NaiveDate>,
        }

        impl Default for Date {
            fn default() -> Date {
                Date {
                    a: SqlDefault::default(),
                    b: None,
                }
            }
        }
    }
}
