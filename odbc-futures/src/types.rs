use odbc_sys::*;
use std::ffi::{CStr, CString};

pub trait SqlType {
    const C_DATA_TYPE: SqlCDataType;
    const SQL_DATA_TYPE: SqlDataType;
}

pub trait SqlDefault: Sized {
    fn default() -> Self;
}

macro_rules! forward_default {
    ($t:ty) => {
        impl SqlDefault for $t {
            fn default() -> $t {
                Default::default()
            }
        }
    };
}

forward_default!(u8);
forward_default!(i8);
forward_default!(u16);
forward_default!(i16);
forward_default!(u32);
forward_default!(i32);
forward_default!(u64);
forward_default!(i64);
forward_default!(f32);
forward_default!(f64);
forward_default!(bool);
forward_default!(usize);
forward_default!(isize);
forward_default!(String);
forward_default!(CString);
forward_default!(Vec<u8>);
forward_default!(Vec<u16>);

#[cfg(feature = "uuid")]
forward_default!(uuid::Uuid);

impl SqlDefault for SqlDataType {
    fn default() -> SqlDataType {
        SQL_UNKNOWN_TYPE
    }
}

impl SqlDefault for Nullable {
    fn default() -> Nullable {
        SQL_NULLABLE_UNKNOWN
    }
}

pub trait SqlPod: SqlType + SqlDefault + Copy + 'static {}

macro_rules! sql_type {
    ($t:ty, $c_type:expr, $sql_type:expr) => {
        impl SqlType for $t {
            const C_DATA_TYPE: SqlCDataType = $c_type;
            const SQL_DATA_TYPE: SqlDataType = $sql_type;
        }
    };
}

macro_rules! sql_pod {
    ($t:ty, $c_type:expr, $sql_type:expr) => {
        sql_type!($t, $c_type, $sql_type);

        impl SqlPod for $t {}
    };
}

sql_pod!(u8, SQL_C_UTINYINT, SQL_EXT_TINYINT);
sql_pod!(i8, SQL_C_STINYINT, SQL_EXT_TINYINT);
sql_pod!(u16, SQL_C_USHORT, SQL_SMALLINT);
sql_pod!(i16, SQL_C_SSHORT, SQL_SMALLINT);
sql_pod!(SqlDataType, SQL_C_SSHORT, SQL_SMALLINT);
sql_pod!(Nullable, SQL_C_SSHORT, SQL_SMALLINT);
sql_pod!(u32, SQL_C_ULONG, SQL_INTEGER);
sql_pod!(i32, SQL_C_SLONG, SQL_INTEGER);
sql_pod!(u64, SQL_C_UBIGINT, SQL_EXT_BIGINT);
sql_pod!(i64, SQL_C_SBIGINT, SQL_EXT_BIGINT);
sql_pod!(f32, SQL_C_FLOAT, SQL_FLOAT);
sql_pod!(f64, SQL_C_DOUBLE, SQL_DOUBLE);
sql_pod!(bool, SQL_C_BIT, SQL_EXT_BIT);

#[cfg(target_pointer_width = "32")]
sql_pod!(usize, SQL_C_ULONG, SQL_INTEGER);
#[cfg(target_pointer_width = "32")]
sql_pod!(isize, SQL_C_SLONG, SQL_INTEGER);

#[cfg(target_pointer_width = "64")]
sql_pod!(usize, SQL_C_UBIGINT, SQL_EXT_BIGINT);
#[cfg(target_pointer_width = "64")]
sql_pod!(isize, SQL_C_SBIGINT, SQL_EXT_BIGINT);

impl<'a> SqlType for &'a str {
    const C_DATA_TYPE: SqlCDataType = SQL_C_WCHAR;
    const SQL_DATA_TYPE: SqlDataType = SQL_EXT_WVARCHAR;
}
sql_type!(str, SQL_C_WCHAR, SQL_EXT_WVARCHAR);
sql_type!(String, SQL_C_WCHAR, SQL_EXT_WVARCHAR);

impl<'a> SqlType for &'a CStr {
    const C_DATA_TYPE: SqlCDataType = SQL_C_CHAR;
    const SQL_DATA_TYPE: SqlDataType = SQL_VARCHAR;
}
sql_type!(CStr, SQL_C_CHAR, SQL_VARCHAR);
sql_type!(CString, SQL_C_CHAR, SQL_VARCHAR);

impl<'a> SqlType for &'a [u8] {
    const C_DATA_TYPE: SqlCDataType = SQL_C_BINARY;
    const SQL_DATA_TYPE: SqlDataType = SQL_EXT_VARBINARY;
}
sql_type!([u8], SQL_C_BINARY, SQL_EXT_VARBINARY);
sql_type!(Vec<u8>, SQL_C_BINARY, SQL_EXT_VARBINARY);

impl<'a> SqlType for &'a [u16] {
    const C_DATA_TYPE: SqlCDataType = SQL_C_WCHAR;
    const SQL_DATA_TYPE: SqlDataType = SQL_EXT_WVARCHAR;
}
sql_type!([u16], SQL_C_WCHAR, SQL_EXT_WVARCHAR);
sql_type!(Vec<u16>, SQL_C_WCHAR, SQL_EXT_WVARCHAR);

#[cfg(feature = "chrono")]
sql_type!(chrono::NaiveTime, SQL_C_TYPE_TIME, SQL_TIME);

#[cfg(feature = "chrono")]
impl SqlDefault for chrono::NaiveTime {
    fn default() -> chrono::NaiveTime {
        chrono::NaiveTime::from_hms(0, 0, 0)
    }
}

#[cfg(feature = "chrono")]
sql_type!(chrono::NaiveDate, SQL_C_TYPE_DATE, SQL_DATE);

#[cfg(feature = "chrono")]
impl SqlDefault for chrono::NaiveDate {
    fn default() -> chrono::NaiveDate {
        chrono::NaiveDate::from_ymd(0, 1, 1)
    }
}

#[cfg(feature = "chrono")]
sql_type!(chrono::NaiveDateTime, SQL_C_TYPE_TIMESTAMP, SQL_TIMESTAMP);

#[cfg(feature = "chrono")]
impl SqlDefault for chrono::NaiveDateTime {
    fn default() -> chrono::NaiveDateTime {
        chrono::NaiveDateTime::new(SqlDefault::default(), SqlDefault::default())
    }
}

#[cfg(feature = "chrono")]
sql_type!(
    chrono::DateTime<chrono::FixedOffset>,
    SQL_C_SS_TIMESTAMPOFFSET,
    SQL_SS_TIMESTAMPOFFSET
);

#[cfg(feature = "chrono")]
impl SqlDefault for chrono::DateTime<chrono::FixedOffset> {
    fn default() -> chrono::DateTime<chrono::FixedOffset> {
        use chrono::Offset;
        Self::from_utc(SqlDefault::default(), chrono::Utc.fix())
    }
}

#[cfg(feature = "uuid")]
sql_type!(uuid::Uuid, SQL_C_GUID, SQL_EXT_GUID);
