use failure::Fail;
use odbc_sys::*;
use std::ffi::NulError;
use std::fmt;
use std::string::FromUtf16Error;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct SqlDiagnosticRecord {
    pub state: String,
    pub message: String,
    pub native_error: SQLINTEGER,
}

impl SqlDiagnosticRecord {
    pub fn is_string_data_right_truncated(&self) -> bool {
        self.state == "01004" || self.state == "22001"
    }

    pub fn is_datetime_field_overflow(&self) -> bool {
        self.state == "22008"
    }

    pub fn is_invalid_precision_scale(&self) -> bool {
        self.state == "HY104"
    }

    pub fn is_decimal_digits_too_large(&self) -> bool {
        self.is_datetime_field_overflow() || self.is_invalid_precision_scale()
    }

    pub fn is_async_canceled_error(&self) -> bool {
        self.state == "HY008"
    }
}

impl fmt::Display for SqlDiagnosticRecord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "(SQLSTATE = \"{}\", {:#X}): \"{}\"",
            self.state, self.native_error, self.message
        )
    }
}

#[derive(Debug, Fail)]
pub enum SqlError {
    FromUtf16Error(#[cause] FromUtf16Error),
    StillExecuting,
    MismatchedParametersType,
    #[cfg(feature = "uuid")]
    BytesError(#[cause] uuid::BytesError),
    NulError(#[cause] NulError),
    OdbcError {
        return_code: SQLRETURN,
        details: Vec<SqlDiagnosticRecord>,
    },
}

impl SqlError {
    pub fn is_async_canceled_error(&self) -> bool {
        match self {
            SqlError::OdbcError { details, .. } => details
                .iter()
                .any(SqlDiagnosticRecord::is_async_canceled_error),
            _ => false,
        }
    }

    pub fn is_invalid_precision_scale(&self) -> bool {
        match self {
            SqlError::OdbcError { details, .. } => details
                .iter()
                .any(SqlDiagnosticRecord::is_invalid_precision_scale),
            _ => false,
        }
    }

    pub fn is_datetime_field_overflow(&self) -> bool {
        match self {
            SqlError::OdbcError { details, .. } => details
                .iter()
                .any(SqlDiagnosticRecord::is_datetime_field_overflow),
            _ => false,
        }
    }

    pub fn is_decimal_digits_too_large(&self) -> bool {
        match self {
            SqlError::OdbcError { details, .. } => details
                .iter()
                .any(SqlDiagnosticRecord::is_decimal_digits_too_large),
            _ => false,
        }
    }
}

pub type SqlResult<T = ()> = Result<T, SqlError>;

impl From<SQLRETURN> for SqlError {
    fn from(return_code: SQLRETURN) -> SqlError {
        assert_ne!(return_code, SQL_SUCCESS);
        assert_ne!(return_code, SQL_SUCCESS_WITH_INFO);
        assert_ne!(return_code, SQL_STILL_EXECUTING);

        SqlError::OdbcError {
            return_code,
            details: vec![],
        }
    }
}

macro_rules! from_cause {
    ($e:ident) => {
        impl From<$e> for SqlError {
            fn from(cause: $e) -> SqlError {
                SqlError::$e(cause)
            }
        }
    };
}

from_cause!(FromUtf16Error);
from_cause!(NulError);

#[cfg(feature = "uuid")]
impl From<uuid::BytesError> for SqlError {
    fn from(cause: uuid::BytesError) -> SqlError {
        SqlError::BytesError(cause)
    }
}

impl fmt::Display for SqlError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::SqlError::*;
        match *self {
            StillExecuting => write!(
                f,
                "Unexpected \"{:?}\" return code in non-async method call",
                SQL_STILL_EXECUTING
            ),
            MismatchedParametersType => write!(
                f,
                "Statement received different SqlParameterProperties from ones that were bound"
            ),
            OdbcError {
                ref return_code,
                ref details,
            } => {
                write!(f, "Return code: \"{:?}\", details:", return_code)?;

                for detail in details.iter() {
                    write!(f, "\n{}", detail)?
                }
                Ok(())
            }
            FromUtf16Error(ref cause) => fmt::Display::fmt(cause, f),
            #[cfg(feature = "uuid")]
            BytesError(ref cause) => fmt::Display::fmt(cause, f),
            NulError(ref cause) => fmt::Display::fmt(cause, f),
        }
    }
}
