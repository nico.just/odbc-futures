use crate::error::*;
use crate::handle::SqlHandle;
use crate::poll::*;
use crate::util;
use crate::util::VecCapacityExt;
use futures::*;
use odbc_sys::*;

pub(crate) trait SqlAttribute: Copy + Send + 'static {
    fn buffer_length(&self) -> Option<SqlAttributeStringLength>;
}

pub(crate) unsafe trait SqlAttributes: SqlHandle {
    type AttributeType: SqlAttribute;
    const GETTER_NAME: &'static str;
    const GETTER: unsafe extern "system" fn(
        Self::Type,
        Self::AttributeType,
        SQLPOINTER,
        SQLINTEGER,
        *mut SQLINTEGER,
    ) -> SQLRETURN;
    const SETTER: unsafe extern "system" fn(
        Self::Type,
        Self::AttributeType,
        SQLPOINTER,
        SQLINTEGER,
    ) -> SQLRETURN;
    const SETTER_NAME: &'static str;

    unsafe fn get_attribute<T>(&self, attribute: Self::AttributeType) -> SqlResult<T>
    where
        T: Copy + Send + 'static,
    {
        get_attribute_impl(self, attribute).unwrap_async()
    }

    unsafe fn get_attribute_async<T>(
        self,
        attribute: Self::AttributeType,
    ) -> SqlValueFuture<Self, T>
    where
        T: Copy + Send + 'static,
    {
        SqlValueFuture::new(self, move |handle| get_attribute_impl(handle, attribute))
    }

    unsafe fn get_attribute_string(&self, attribute: Self::AttributeType) -> SqlResult<String> {
        let mut buffer: Vec<u16> = Vec::with_capacity(SQL_MAX_MESSAGE_LENGTH as usize);
        let mut len: SQLINTEGER = 0;

        get_attribute_string_impl(self, attribute, &mut buffer, &mut len).unwrap_async()
    }

    unsafe fn get_attribute_string_async(
        self,
        attribute: Self::AttributeType,
    ) -> SqlValueFuture<Self, String> {
        let mut buffer: Vec<u16> = Vec::with_capacity(SQL_MAX_MESSAGE_LENGTH as usize);
        let mut len: SQLINTEGER = 0;

        SqlValueFuture::new(self, move |handle| {
            get_attribute_string_impl(handle, attribute, &mut buffer, &mut len)
        })
    }

    unsafe fn set_attribute(&mut self, attribute: Self::AttributeType, value: usize) -> SqlResult {
        set_attribute_impl(self, attribute, value).unwrap_async()
    }

    unsafe fn set_attribute_async(
        self,
        attribute: Self::AttributeType,
        value: usize,
    ) -> SqlFuture<Self> {
        let f = SqlValueFuture::new(self, move |handle| {
            set_attribute_impl(handle, attribute, value)
        });

        flatten_value_future(f)
    }

    unsafe fn set_attribute_string(
        &mut self,
        attribute: Self::AttributeType,
        value: &str,
    ) -> SqlResult {
        let buffer: Vec<u16> = value.encode_utf16().collect();

        set_attribute_string_impl(self, attribute, &buffer).unwrap_async()
    }

    unsafe fn set_attribute_string_async(
        self,
        attribute: Self::AttributeType,
        value: &str,
    ) -> SqlFuture<Self> {
        let buffer: Vec<u16> = value.encode_utf16().collect();

        let f = SqlValueFuture::new(self, move |handle| {
            set_attribute_string_impl(handle, attribute, &buffer)
        });

        flatten_value_future(f)
    }
}

unsafe fn get_attribute_impl<T, H>(handle: &H, attribute: H::AttributeType) -> SqlPoll<T>
where
    H: SqlAttributes,
    T: Copy + Send + 'static,
{
    let mut value: T = std::mem::uninitialized();
    let value_ptr: *mut T = &mut value;
    let buffer_length = attribute
        .buffer_length()
        .expect("buffer_length() should be not empty for non-string values");

    let ret = H::GETTER(
        handle.typed_handle(),
        attribute,
        value_ptr as SQLPOINTER,
        buffer_length as i32,
        std::ptr::null_mut(),
    );
    match ret {
        SQL_SUCCESS | SQL_SUCCESS_WITH_INFO => Ok(Async::Ready(value)),
        SQL_STILL_EXECUTING => Ok(Async::NotReady),
        SQL_ERROR => {
            let e = handle.get_detailed_error(ret);
            Err(e)
        }
        _ => panic!("Unexpected {} return code: {:?}", H::GETTER_NAME, ret),
    }
}

unsafe fn get_attribute_string_impl<H: SqlAttributes>(
    handle: &H,
    attribute: H::AttributeType,
    buffer: &mut Vec<u16>,
    len: &mut SQLINTEGER,
) -> SqlPoll<String> {
    assert!(
        attribute.buffer_length().is_none(),
        "buffer_length() should be empty for string values"
    );
    loop {
        let ret = H::GETTER(
            handle.typed_handle(),
            attribute,
            buffer.as_mut_ptr() as SQLPOINTER,
            buffer.capacity_bytes() as SQLINTEGER,
            len,
        );

        match ret {
            SQL_SUCCESS | SQL_SUCCESS_WITH_INFO => {
                assert!(*len >= 0);
                let mut len = *len as usize;
                let size = std::mem::size_of::<u16>();
                assert_eq!(0, len % size);
                len /= size;

                if util::is_string_data_right_truncated(handle, ret)? {
                    buffer.reserve_capacity(len + 1)
                } else {
                    buffer.set_len_checked(len);
                    return util::from_utf_16_null_terminated(&buffer)
                        .map(Async::Ready)
                        .map_err(SqlError::from);
                }
            }
            SQL_STILL_EXECUTING => return Ok(Async::NotReady),
            SQL_ERROR => return Err(handle.get_detailed_error(ret)),
            _ => panic!("Unexpected {} return code: {:?}", H::GETTER_NAME, ret),
        }
    }
}

unsafe fn set_attribute_impl<H: SqlAttributes>(
    handle: &mut H,
    attribute: H::AttributeType,
    value: usize,
) -> SqlPoll {
    let buffer_length = attribute
        .buffer_length()
        .expect("buffer_length() should be not empty for non-string values");
    let ret = H::SETTER(
        handle.typed_handle(),
        attribute,
        value as SQLPOINTER,
        buffer_length as i32,
    );
    match ret {
        SQL_SUCCESS | SQL_SUCCESS_WITH_INFO => Ok(Async::Ready(())),
        SQL_STILL_EXECUTING => Ok(Async::NotReady),
        SQL_ERROR => {
            let e = handle.get_detailed_error(ret);
            Err(e)
        }
        _ => panic!("Unexpected {} return code: {:?}", H::SETTER_NAME, ret),
    }
}

unsafe fn set_attribute_string_impl<H: SqlAttributes>(
    handle: &mut H,
    attribute: H::AttributeType,
    buffer: &[u16],
) -> SqlPoll {
    assert!(
        attribute.buffer_length().is_none(),
        "buffer_length() should be empty for string values"
    );
    let ret = H::SETTER(
        handle.typed_handle(),
        attribute,
        buffer.as_ptr() as SQLPOINTER,
        std::mem::size_of_val(buffer) as SQLINTEGER,
    );

    match ret {
        SQL_SUCCESS | SQL_SUCCESS_WITH_INFO => Ok(Async::Ready(())),
        SQL_STILL_EXECUTING => Ok(Async::NotReady),
        SQL_ERROR => {
            let e = handle.get_detailed_error(ret);
            Err(e)
        }
        _ => panic!("Unexpected {} return code: {:?}", H::SETTER_NAME, ret),
    }
}
