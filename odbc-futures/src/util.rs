use crate::error::{SqlDiagnosticRecord, SqlResult};
use crate::handle::SqlHandle;
use odbc_sys::*;
use std::string::FromUtf16Error;

pub fn decimal_digits(parameter_type: SqlDataType) -> SQLSMALLINT {
    match parameter_type {
        SQL_TIMESTAMP
        | SQL_SS_TIMESTAMPOFFSET
        | SQL_TIME
        | SQL_DATETIME
        | SQL_SS_TIME2
        | SQL_EXT_TIMESTAMP
        | SQL_EXT_TIME_OR_INTERVAL => 7,
        _ => 0,
    }
}

pub fn truncate_nanoseconds(mut fraction: SQLUINTEGER, decimal_digits: SQLSMALLINT) -> SQLUINTEGER {
    const MAX_DIGITS_NANOSECOND: SQLSMALLINT = 9;
    assert!(
        decimal_digits >= 0 && decimal_digits <= MAX_DIGITS_NANOSECOND,
        "number of digits not in range (for nanoseconds): {} (should be from 0 to {})",
        decimal_digits,
        MAX_DIGITS_NANOSECOND
    );
    let power = MAX_DIGITS_NANOSECOND - decimal_digits;
    let factor = 10u32.pow(power as u32);

    fraction /= factor;
    fraction *= factor;
    fraction
}

pub fn from_utf_16_null_terminated(mut slice: &[u16]) -> Result<String, FromUtf16Error> {
    if let Some(&0) = slice.last() {
        slice = &slice[..slice.len() - 1];
    };

    String::from_utf16(slice)
}

pub fn u32_to_bool(x: u32) -> bool {
    match x {
        1 => true,
        0 => false,
        _ => panic!("{} is not a valid boolean value (0/1)", x),
    }
}

pub fn is_string_data_right_truncated<T: SqlHandle>(handle: &T, ret: SQLRETURN) -> SqlResult<bool> {
    match ret {
        SQL_SUCCESS | SQL_NO_DATA => Ok(false),
        SQL_SUCCESS_WITH_INFO => {
            let diagnostics = handle.diagnostics()?;
            Ok(diagnostics
                .iter()
                .any(SqlDiagnosticRecord::is_string_data_right_truncated))
        }
        _ => panic!("Unexpected SQLRETURN: {:?}", ret),
    }
}

pub trait VecCapacityExt {
    fn reserve_capacity(&mut self, capacity: usize);
    unsafe fn set_len_checked(&mut self, len: usize);
    fn capacity_bytes(&self) -> usize;
    fn remaining_capacity(&self) -> usize;
    fn remaining_capacity_bytes(&self) -> usize;
}

impl<T> VecCapacityExt for Vec<T> {
    fn reserve_capacity(&mut self, capacity: usize) {
        let len = self.len();
        if capacity > len {
            self.reserve_exact(capacity - len);
        }
    }
    unsafe fn set_len_checked(&mut self, len: usize) {
        assert!(len <= self.capacity());
        self.set_len(len);
    }
    fn capacity_bytes(&self) -> usize {
        self.capacity() * std::mem::size_of::<T>()
    }
    fn remaining_capacity(&self) -> usize {
        self.capacity() - self.len()
    }
    fn remaining_capacity_bytes(&self) -> usize {
        self.remaining_capacity() * std::mem::size_of::<T>()
    }
}

#[cfg(test)]
mod tests {
    use crate::handle::SqlHandle;
    use std::sync::{Arc, RwLock};

    pub fn print_diagnostics<T: SqlHandle>(handle: &T) {
        let diagnostics = handle.diagnostics().unwrap();

        let mut iter = diagnostics.iter().peekable();
        while let Some(detail) = iter.next() {
            if iter.peek().is_some() {
                println!("{}\n", detail);
            } else {
                println!("{}", detail);
            }
        }
    }

    pub fn print_diagnostics_async<T: SqlHandle>(handle: &Arc<RwLock<T>>) {
        let lock = handle.read().unwrap();

        print_diagnostics(&lock as &T);
    }
}

#[cfg(test)]
pub use self::tests::*;
